# Rappel IMPORTANT
un pilote se doit d'avoir à bord "la documentation" adéquate pour réaliser le vol envisagé.


# CUPBOARD

CupBoard: Outil d'édition et de création de fichiers .cup
* Lecture d'un fichier .cup, le fichier apparait dans un onglet : copier l'onglet "trame" pour créer une nouvelle liste de points de virages et renommer l'onglet

* Outils de conversion des coordonnées depuis et vers le format de coordonnées .cup

* Liens vers Google maps et Géoportail dans chaque onglet avec outils de conversion en dessous

* Une fois édité, la liste de points de virages peut être simplement copier-coller vers un autre onglet

* Possibilité d'édition et de création de circuit: copier l'onglet "trameCirc" pour créer une nouvelle liste de circuit et renommer l'onglet

* Ecriture des fichiers .cup



# ALPES
Champs vachables des alpes :
* Créé à partir du "Guide des aires de sécurité dans les Alpes" Nouvelle présentation Edition 1

* différent fichiers sont créés en fonction de la difficulté (couleur) des champs vachables

# Club AACM
Points de virages AACM :
* AACM2018 contient :
	* Points de virages pour les circuits club
	* Points de virages pour les sprints
	* Tous les circuits du club
	* Terrains ULM compatible pour le Nimbus 3D
	* Tous les champs vachables des alpes

* (Recommandé) AACM2018FranceAPT contient en plus :
	* Aérodromes de France
		* La source provient de XCSoar
		* Le fichier a été nettoyé pour ne garder que les aérodromes

* FranceAPT contient seulement :
	* Aérodromes de France
		* La source provient de XCSoar
		* Le fichier a été nettoyé pour ne garder que les aérodromes
